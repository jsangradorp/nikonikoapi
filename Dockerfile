FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

ARG dbhost
ARG dbuser
ARG dbpassword

ENV DB_HOST=$dbhost DB_USERNAME=$dbuser DB_PASSWORD=$dbpassword
CMD [ "uwsgi", "./conf/etc/uwsgi.ini"]
