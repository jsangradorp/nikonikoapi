\set providedpassword `echo ${DB_PASSWORD:-terriblepassword}`
CREATE USER nikoniko WITH PASSWORD :'providedpassword';
CREATE DATABASE nikoniko;
GRANT ALL PRIVILEGES ON DATABASE nikoniko TO nikoniko;
CREATE DATABASE initdone;
